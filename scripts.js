//Works Settings
var HowManyProjects = 2;
var ImageExtension = "jpg";
var ImageContainerId = "works-container";
var scrollDuration = 200;





var footerClickHeight   = "50%";
var footerOverflow      = "scroll"; //switches from none to value and back
var selectedElement     = "[<-]";
var notSelectedElement  = "[->]";
var commentClass = "comments";
var selectedClass = "selected";
var codeContainer = "scroll-container";
var codeClass = "code";

//first element matched will be changed (if there are more than one)
var bgElement = "html";
var bgColorInput = "bgColor";
var textElement = "html";
var textColorInput = "textColor";


var htmlTemptText = ""; //empty global string


//getter by id
function getE(element) {
var object = document.getElementById(element);
return object;
}

//setter by id with property = value
function set(element,prop,val) {
  var o = getE(element);
  o.style[prop] = val;
}


// changing symbol of active menu li
var menuList = document.getElementById("menu");
var open = "news";
var closeStart = false;
var start = "news";


menuList.addEventListener('click', function(e) {
    if (e.target.tagName === 'LI'){
      var show = e.target.className;
      var tempClass = e.target.className;
      if(e.target.getAttribute("data-attr") == notSelectedElement) {
        e.target.setAttribute("data-attr",selectedElement);
        e.target.setAttribute("id",selectedClass);
        var targetElement = document.getElementById(show);
        targetElement.style.display = "block";
        if(open != show) {
        document.getElementById(open).style.display = "none";
        var openElement = menuList.getElementsByClassName(open)[0];
        if(typeof openElement != 'undefined') {
          openElement.setAttribute("data-attr",notSelectedElement);
          openElement.removeAttribute("id");
          document.getElementById(start).style.display = "none";
        }
        open = show;
          if(e.target.className === "main") {
            console.log(getWidth(codeClass));
            var codeWidth = getWidth(codeClass);
            document.getElementById(codeContainer).style.width = codeWidth+"px";
          }
        }
        closeStart = true;
      }
      else if(e.target.getAttribute("data-attr") == selectedElement)
      {
        e.target.setAttribute("data-attr",notSelectedElement);
        e.target.removeAttribute("id");
        document.getElementById(show).style.display = "none";
        closeStart = false;
      }
      if(closeStart === true) {
        document.getElementById(start).style.display= "none";
      }
      else document.getElementById(start).style.display= "block";
    }
});


// onClick on footer-Element will change its height according to var at the top
document.getElementById("news").onclick = function () {
  var footerClickTempHeight = this.style.height;
  var footerTempOverflow = this.style.overflowY;
  this.style.height = footerClickHeight;
  this.style.overflowY = footerOverflow;
  footerClickHeight = footerClickTempHeight;
  footerOverflow = footerTempOverflow;
}


//Background Color Changer
function changeBg(bgElement, bgColorInput) {
  var color = document.getElementById(bgColorInput).value;
  document.getElementsByTagName(bgElement)[0].style.backgroundColor = "#"+color;
  var textColor = ('000000' + (('0xffffff' ^ '0x'+color).toString(16))).slice(-6);
  document.getElementsByTagName(bgElement)[0].style.color = "#"+textColor;
  console.log(color);
  console.log(textColor);
}
document.getElementById(bgColorInput).onkeyup = function() {
  changeBg(bgElement, bgColorInput)
};

function changeText(textElement, textColorInput) {
  var color = document.getElementById(textColorInput).value;
  document.getElementsByTagName(textElement)[0].style.color = "#"+color;
  console.log(document.getElementsByTagName(textElement)[0].style.color)
}
document.getElementById(textColorInput).onchange = function() {
  changeText(textElement, textColorInput);
};


//escpae HTML chars
function escapeHtml(text) {
  var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };

  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}

//set Code-Container width (maybe possible in plain CSS4)
function getWidth(className) {
  var codeElements = document.getElementsByTagName(className);
  var containerWidth = 0;

  for (var i = 0; i < codeElements.length; i++) {
    console.log(codeElements[i].clientWidth);
    containerWidth = parseInt(containerWidth) + parseInt(codeElements[i].offsetWidth);
  }
  containerWidth = containerWidth + 50;
  return containerWidth;
}

//onLoad: footer content + main code ajax request
window.onload = function () {
  o = getE("news");
  loadDoc("footer.html", function(responseText) {
    o.innerHTML = responseText;
  });
  p = getE("main");
    var htmlString = "";
  loadDoc("index.html", function(responseText) {
    htmlString = "<div id='scroll-container'>"+
                  "<pre class='code'>"+
                    "<h1>HTML:</h1>"+
                    "<code>"+escapeHtml(responseText)+"</code>"+
                  "</pre>";
    var regEx = /(\&lt;!--)([\s\S]*?)(--\&gt;)/gm;
    htmlString =
    htmlString.replace(regEx,"<span class='"+commentClass+"'>$1$2$3</span>");

  });
  loadDoc("scripts.js", function(responseText) {
    htmlString += "<pre class='code'>"+
                    "<h1>JavaScript:</h1>"+
                    "<code>"+escapeHtml(responseText)+"</code>"+
                    "</pre>\n";
  });
  loadDoc("style.css", function(responseText) {
    htmlString += "<pre class='code'>"+
                    "<h1>CSS:</h1>"+
                    "<code>"+escapeHtml(responseText)+"</code>"+
                  "</pre>\n</div>";
    // var regExp = /(\/\/)(.*)(\n)/g;
    var regEx = /(\/\*[\s\S]*?\*\/)|(([^\\:]|^)\/\/.*)/gm;
    htmlString =
    htmlString.replace(regEx, "<span class='"+commentClass+"'>$1$2</span>\n");
    p.innerHTML = htmlString;
  });
}
//worksLoader
for(var i=1; i<=HowManyProjects;i++) {
  if(i<10) folder = "0"+i;
  else folder = i;
  var image = 1;
  console.log(folder);
        loadDoc("./works/"+folder+"/works.txt", function(responseText) {
          if(image<10) var folder = "0"+image;
          else var folder = image;

          console.log(image);
        var imagetexts = responseText.split('"<<";');
        var  worksHTML = "<article  class='single-works' id='w"+folder+"'onscroll='scrollWorks(this)'>"
        +"<form-object class='arrowPrev' onclick='scrollPrev(\"w"+folder+"\")'></form-object>";

        for(var i=1;i<=imagetexts.length;i++) {
          var path = "./works/"+folder+"/"+i+"."+ImageExtension;
          worksHTML += "<image-container id='"+folder+"_"+i+"' style='background-image: url("+path+")'>"
                      +"<label for='IMG"+folder+"_"+i+"'>"+imagetexts[i-1]+"</label>"
                      +"<img id='IMG"+folder+"_"+i+"' src='"+path+"'>"
                      +"</image-container>";
        }
        worksHTML += "<form-object class='arrowNext' onclick='scrollNext(\"w"+folder+"\")'></form-object>"
        +"</article>";
        document.getElementById(ImageContainerId).innerHTML += worksHTML;
        scrollOffset["w"+folder] = 0;
        image = image+1;
  });
}


var scrollOffset = Array();
function scrollWorks(element) {
  scrollOffset[element.id] = element.scrollLeft;
  element.getElementsByTagName("form-object")[0].style.left = scrollOffset[element.id]+"px";
  element.getElementsByTagName("form-object")[1].style.right = "-"+scrollOffset[element.id]+"px";
}

var imageScrollOffset = 0;

function scrollNext(id) {
  var e = document.getElementById(id);
  var elementsInScrollContainer = e.getElementsByTagName("image-container").length;
  var scrollContainerMaxWidth = e.clientWidth*(elementsInScrollContainer-1);
  // console.log("SCMW:"+scrollContainerMaxWidth);
  if(Math.floor(scrollOffset[e.id]+e.clientWidth)<Math.ceil(scrollContainerMaxWidth+e.clientWidth)-1)
  {
    scrollOffset[e.id] = Math.floor(scrollOffset[e.id]+e.clientWidth)+e.clientWidth/3;
    scrollOffset[e.id] = Math.floor(scrollOffset[e.id]/e.clientWidth)*e.clientWidth;
    // scrollTo[i] = e.scrollLeft;
    for(var i=0;i<=10;i=i+1){
      var interval = Math.log(i)*30;
      if(i>0) {
      scrollTo[i] = Math.round(scrollOffset[e.id]-e.clientWidth)+e.clientWidth/10*i;
      if(scrollTo[i]<e.scrollLeft) scrollTo[i] = e.scrollLeft;
      setTimeout(function (element,scrollTo) {
        element.scrollLeft = scrollTo;
      },interval, e, scrollTo[i]);
    }
    }
  }


}


  var scrollTo = Array("");
function scrollPrev(id) {
  var e = document.getElementById(id);
  var elementsInScrollContainer = e.getElementsByTagName("image-container").length;
  var scrollContainerMaxWidth = e.clientWidth*(elementsInScrollContainer-1);
  if(e.scrollLeft>0) {
  if(Math.ceil(scrollOffset[e.id])-e.clientWidth>=(-e.clientWidth/1))
  {
    scrollOffset[e.id] = Math.ceil(scrollOffset[e.id]-e.clientWidth)-e.clientWidth/3;
    scrollOffset[e.id] = Math.ceil(scrollOffset[e.id]/e.clientWidth)*e.clientWidth;
    // e.scrollLeft = scrollOffset;

    for(var i=0;i<=10;i=i+1){
      var interval = Math.log(i)*30;
      if(i>0) {

      scrollTo[i] = Math.round(scrollOffset[e.id]+e.clientWidth)-e.clientWidth/10*i;
      if(scrollTo[i]>e.scrollLeft) scrollTo[i] = e.scrollLeft;
      setTimeout(function (element,scrollTo) {
        element.scrollLeft = scrollTo;
      },interval, e, scrollTo[i]);
    }
    }

  }
}
}



//file load per ajax request
function loadDoc(file, callback) {
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
    if(typeof callback === 'function')
   callback(this.responseText);
  }
};
xhttp.open("GET", file, true);
xhttp.send();
}

//check if file exists
function fileExists(url)
{
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
         // Action to be performed when the document is read;

      }
  };
  xhttp.open("GET", "filename", true);
  xhttp.send();
}
